#!/bin/bash

cd ..

Home="."
libDir=${Home}/lib
CP=${Home}/out/production/rddlsim
CYGWIN_SEP=";"
UNIX_SEP=":"

# Choose separator as appropriate for shell system (Cygwin, otherwise UNIX)
SEP=":" 
if [[ $OSTYPE == "cygwin" ]] ; then
    SEP=";" 
fi

for i in ${libDir}/*.jar ; do
    CP="${CP}${SEP}$i"
done


# echo "Start!"
# DIR='./huTest/log/random'
# while read -r inst
# do
# 	echo $inst
# 	date "+%Y-%m-%d %H:%M" | tee $DIR/$inst.log
# 	java -Xms100M -Xmx2048M -classpath $CP rddl.sim.Simulator files/final_comp/rddl rddl.policy.RandomBoolPolicy $inst | tee -a  $DIR/$inst.log
# 	date "+%Y-%m-%d %H:%M" | tee -a $DIR/$inst.log
# done < ./huTest/test-case-list.txt


echo "Start!"
DIR='./huTest/log/uct-m'
while read -r inst
do
	echo $inst
	date "+%Y-%m-%d %H:%M" > $DIR/$inst.log
	java -Xms100M -Xmx2048M -classpath $CP rddl.sim.Simulator files/final_comp/rddl rddl.solver.mdp.uct.UCT $inst >>  $DIR/$inst.log
	date "+%Y-%m-%d %H:%M" >> $DIR/$inst.log
done < ./huTest/test-case-list-short.txt

# echo "Start!"
# DIR='./huTest/log/rtdp'
# while read -r inst
# do
# 	echo $inst
# 	date "+%Y-%m-%d %H:%M" > $DIR/$inst.log
# 	java -Xms100M -Xmx2048M -classpath $CP rddl.sim.Simulator files/final_comp/rddl rddl.solver.mdp.rtdp.RTDP $inst >>  $DIR/$inst.log
# 	date "+%Y-%m-%d %H:%M" >> $DIR/$inst.log
# done < ./huTest/test-case-list.txt

# echo "Start!"
# DIR='./huTest/log/random-m'
# while read -r inst
# do
# 	echo $inst
# 	date "+%Y-%m-%d %H:%M" | tee $DIR/$inst.log
# 	java -Xms100M -Xmx2048M -classpath $CP rddl.sim.Simulator files/final_comp/rddl rddl.policy.RandomBoolPolicy $inst | tee -a  $DIR/$inst.log
# 	date "+%Y-%m-%d %H:%M" | tee -a $DIR/$inst.log
# done < ./huTest/test-case-list.txt