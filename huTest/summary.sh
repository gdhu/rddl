#!/bin/bash

for file in ./*/rtdp-m/*
do
    if test -f $file
    then
        echo $(basename $file) `grep 'total_avg_reward' $file`
    else
        echo $file 是目录
    fi
done