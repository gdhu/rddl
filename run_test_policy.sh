#!/bin/bash

Home="."
libDir=${Home}/lib
CP=${Home}/out/production/rddlsim
CYGWIN_SEP=";"
UNIX_SEP=":"

# Choose separator as appropriate for shell system (Cygwin, otherwise UNIX)
SEP=":" 
if [[ $OSTYPE == "cygwin" ]] ; then
    SEP=";" 
fi

for i in ${libDir}/*.jar ; do
    CP="${CP}${SEP}$i"
done

DATE=`date "+%Y-%m-%d-%H-%M"`

java -Xms100M -Xmx2048M -classpath $CP rddl.sim.Simulator files/final_comp/rddl rddl.solver.mdp.uct.UCT elevators_inst_mdp__9 | tee  elevators_inst_mdp__9_$DATE.log
